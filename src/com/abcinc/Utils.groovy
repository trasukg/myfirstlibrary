
package com.abcinc;

def checkout(def repo) {
	node {
		stage "Checkout"
		git url:"https://trasukg@bitbucket.org/trasukg/simplegreeting.git"
	}
}

def mvn_install() {
	node {
		stage 'Install'
		bat "mvn install"
	}
}

def mvn_clean() {
	node {
		stage 'Clean'
		bat "mvn clean"
	}
}

def mvn_verify() {
	node {
		stage "Verify"
		bat "mvn verify"
	}
}

def archive_reports() {
	node {
		stage "Archive Reports"
		step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST*.xml']);	
	}
}
def user_acceptance(wsdir) {
		stage 'User Acceptance'
		def response=input message: 'Is this build good to go?',
		 	parameters: [choice( choices: 'Yes/nNo',
		 	  description: "", name: "Pass")]
		if (response=="Yes") {
			node {
				stage "Deploy"
				
				bat "xcopy \"C:\\Program Files\\Jenkins\\$wsdir\\target\\SimpleGreeting.jar\" C:\\workspace\\dev\\ /y"
				
			}
		}
}

def standardProcess() {
	checkout();
	mvn_clean();
	mvn_install();
	mvn_verify();
	user_acceptance("ABCInc")
}
